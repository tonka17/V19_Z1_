package com.example.antonija.v19_z2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends Activity {

    private final Random _RandomGenerator = new Random();
    final int [] quotearray = new int[]{R.string.QuoteOne, R.string.QuoteTwo, R.string.QuoteThree, R.string.QuoteFour, R.string.QuoteFive, R.string.QuoteSix, R.string.QuoteSeven, R.string.QuoteEight, R.string.QuoteNine};
    final int [] colorbackgroundarray = new int []{R.color.colorBackgroundOne, R.color.colorBackgroundTwo, R.color.colorBackgroundThree, R.color.colorBackgroundFour, R.color.colorBackgroundFive};
    final int [] colortextarray = new int []{R.color.colorTextOne, R.color.colorTextTwo, R.color.colorTextThree, R.color.colorTextFour, R.color.colorTextFive};
    final int [] imagearray = new int []{R.drawable.photoone, R.drawable.phototwo, R.drawable.photothree, R.drawable.photofour, R.drawable.photofive, R.drawable.photosix, R.drawable.photoseven, R.drawable.photoeight, R.drawable.photonine};

    int count = 0;
    int randomcount;

    @BindView(R.id.tvQuote) TextView tvQuote;
    @BindView(R.id.myLayout) RelativeLayout myLayout;
    @BindView(R.id.ivImage) ImageView ivImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        change(_RandomGenerator, quotearray, colortextarray, colorbackgroundarray, imagearray);
        ButterKnife.bind(this);
    }
    public int generateTime()
    {
        int time = 5000 + this._RandomGenerator.nextInt(12000-4000);
        return time;
    }

    public void change(final Random _RandomGenerator, final int[] quotearray, final int [] colortextarray, final int [] colorbackgroundarray, final int [] imagearray)
    {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run()
            {
                if(count < quotearray.length-1)
                {
                    count++;
                }
                else
                    {
                count = 0;
                }

                tvQuote.setText(quotearray[count]);
                ivImage.setImageResource(imagearray[count]);
                randomcount = _RandomGenerator.nextInt(colortextarray.length);
                tvQuote.setTextColor(getResources().getColor(colortextarray[randomcount]));
                randomcount = _RandomGenerator.nextInt(colorbackgroundarray.length);
                myLayout.setBackgroundColor(getResources().getColor(colorbackgroundarray[randomcount]));

                change(_RandomGenerator, quotearray, colortextarray, colorbackgroundarray, imagearray);
                }
        }, generateTime());
    }
}



